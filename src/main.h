#ifndef _MAIN_H
#define _MAIN_H

#include <string>

int const NO_COLOR = -1;
int const DEFAULT = 1;
int const BLACK= 2;
int const RED = 3;
int const GREEN = 4;
int const YELLOW = 5;
int const BLUE = 6;
int const MAGENTA = 7;
int const CYAN = 8;
int const WHITE = 9;
int const BLACK_BACK = 10;
int const RED_BACK = 11;
int const GREEN_BACK = 12;
int const YELLOW_BACK = 13;
int const BLUE_BACK = 14;
int const MAGENTA_BACK = 15;
int const CYAN_BACK = 16;
int const WHITE_BACK = 17;

int getColor(std::string colorName);

float randomFloat(float min, float max);
int randomInt(int min, int max);
void randomDir(float& dirY, float& dirX);

#endif
