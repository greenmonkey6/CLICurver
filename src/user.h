#ifndef _USER_H
#define _USER_H

#include <string>

/**
 * Represents an actual person playing the game
 */
class User {
	private:
		/** Name of the user */
		std::string name;

		/** Color index of NCurses pair all text of the user is drawn in */
		int color;

		/** Keys to change the direction of the player */
		char left, straight, right;

		/** Score of the user it can collect by winning games */
		int score;

	public:
		explicit User(std::string name, int color, char left, char straight, char right);

		/**
		 * Return the name of the user
		 */
		std::string getName();

		/**
		 * Return the color pair index of the user
		 */
		int getColor();

		/**
		 * Returns the key that can be used to move the player left
		 */
		char getLeftKey();
		/**
		 * Returns the key that can be used to move the player straight
		 */
		char getStraightKey();
		/**
		 * Returns the key that can be used to move the player right
		 */
		char getRightKey();

		/**
		 * Returns the score of the user
		 */
		int getScore();

		/**
		 * Adds the given number to the score of the user
		 */
		void addScore(int score);
};

#endif
