#include <ncurses.h>
#include <cmath>

#include "map.h"
#include "main.h"

Map::Map(WINDOW* window, int playerCount) :
	TILE_SIZE_Y(2.0f),
	TILE_SIZE_X(1.0f),
	MAX_OCCUPATION_AGE(5) {
	this->window = window;

	// Gets window size
	int height, width;
	getmaxyx(window, height, width);

	// Initializes field
	// TODO Maybe find better way to initialize
	Tile tile = {' ', NO_GAME_OBJECT, 0};
	std::vector<Tile> row(width, tile);
	field.resize(height);
	for (auto it = field.begin(); it < field.end(); ++it) {
		(*it) = row;
	}

	// Draws border around the map
	for (int i = 0; i < height; i++) {
		draw('#', i, 0, DEFAULT, NO_GAME_OBJECT);
		draw('#', i, width - 1, DEFAULT, NO_GAME_OBJECT);
	}
	for (int i = 0; i < width; i++) {
		draw('#', 0, i, DEFAULT, NO_GAME_OBJECT);
		draw('#', height - 1, i, DEFAULT, NO_GAME_OBJECT);
	}
}

inline void Map::drawChar(char character, int y, int x, int color) {
//	wattron(window, A_BOLD);
	wattron(window, COLOR_PAIR(color));
	mvwaddch(window, y, x, character);
	wattroff(window, COLOR_PAIR(color));
//	wattroff(window, A_BOLD);
}

void Map::draw(char character, int y, int x, int color, GameObject* caller, bool temp) {
	// Checks if an actual game object is drawing
	if (caller != NO_GAME_OBJECT) {
		// Checks if the game object draws in a new tile
		if (fieldAt(y, x).owner != caller) {
			// Increases game object's age
			gameObjectData[caller].age++;
		}
	}

	// Clears temporary tiles if the corresponding game object is drawing.
	for (auto it = temps.begin(); it < temps.end(); ++it) {
		if (it->owner == caller) {
			// TODO Implement proper way to get the color. Maybe the color argument is not needed in draw, if the game object is already given. (Instead of saving it in the tile)
			drawChar(field[it->y][it->x].character, it->y, it->x, field[it->y][it->x].color);
			temps.erase(it);
		}
	}

	// Draws character to the screen
	drawChar(character, y, x, color);
	// Draws character to the field
	if (!temp) {
		fieldAt(y, x).character = character;
		fieldAt(y, x).owner = caller;
		fieldAt(y, x).age = gameObjectData[caller].age;
		fieldAt(y, x).color = color;
		draws[caller].push_back({&fieldAt(y, x), y, x});
	}

	// Adds the tile to the list of tiles that should get cleared on the next draw
	if (temp) {
		temps.push_back({caller, y, x});
	}
}

Axis Map::rasterizeLinePoint(float inY, float inX, float dirY, float dirX, float& outY, float& outX) {
	float mid1, mid2;
	float dir1, dir2;
	float in1, in2;
	Axis axis;
	// Decides the axis we need to rasterize along and sets variables correspondingly
	if (abs(dirY / dirX) < getTileDiagonalSlope()) {
		dir1 = dirX;
		dir2 = dirY;
		in1 = inX;
		in2 = inY;
		axis = Axis::X;
	} else {
		dir1 = dirY;
		dir2 = dirX;
		in1 = inY;
		in2 = inX;
		axis = Axis::Y;
	}

	// Calculates point that lays in the middle of the left and right neighbor of the input point
	mid1 = toInternalCoordsAxis(((int) (toScreenCoordsAxis(in1, axis)) + 0.5f), axis);
	// Interpolates linearly to get the second value of the point
	mid2 = (dir2 / dir1) * (mid1 - in1) + in2;

	// Sets output depending on the axis
	outY = toScreenCoordsY(axis == Axis::X ? mid2 : mid1);
	outX = toScreenCoordsX(axis == Axis::X ? mid1 : mid2);

	return axis;
}

char Map::getLineChar(float dirY, float dirX, float diff, Axis axis) {
	char character = 'O';

	// We use a character for an direction as shown in the figure below:
	/*
	 * \        |        /
	 *
	 *          |
	 *          |
	 * -   -----|--x--   -
	 *          y
	 *          |
	 *
	 * /        |        \
	 */

	// Normalizes the tile diagonal vector
	static float const TILE_DIAG_LENGTH = sqrt(TILE_SIZE_Y * TILE_SIZE_Y + TILE_SIZE_X * TILE_SIZE_X);
	static float const TILE_SIZE_Y_NORM(TILE_SIZE_Y / TILE_DIAG_LENGTH);
	static float const TILE_SIZE_X_NORM(TILE_SIZE_X / TILE_DIAG_LENGTH);

	// Computes the x-component of the (normalized) angle bisector of the tile diagonal and the up vector to get the boundary between the '|' and the '/' characters. The angle bisection is the sum of both vector (because they have the same length).
	static float const BOUND_UD_Y = TILE_SIZE_Y_NORM + 1.0f;
	static float const BOUND_UD_X = TILE_SIZE_X_NORM + 0.0f;
	static float const BOUND_UD_LENGTH = sqrt(BOUND_UD_Y * BOUND_UD_Y + BOUND_UD_X * BOUND_UD_X);
	static float const BOUND_UD_X_NORM = BOUND_UD_X / BOUND_UD_LENGTH;

	// Computes the y-component of the same vector for the '/' and the '-' characters.
	static float const BOUND_DR_Y = TILE_SIZE_Y_NORM + 0.0f;
	static float const BOUND_DR_X = TILE_SIZE_X_NORM + 1.0f;
	static float const BOUND_DR_LENGTH = sqrt(BOUND_DR_Y * BOUND_DR_Y + BOUND_DR_X * BOUND_DR_X);
	static float const BOUND_DR_Y_NORM = BOUND_DR_Y / BOUND_DR_LENGTH;

	// Chooses the '|' character inside of the cone spanned by the first above vector and its mirror image on the y-axis. The same for the '-' character and the second above vector with the x-axis. The '/' and '\' character are chosen based on the quadrant.
	// Each sector gets separated into three subcases that depend on the distance to the middle of the tile.
	if (-BOUND_UD_X_NORM < dirX && dirX < BOUND_UD_X_NORM) {
		character = '|';
	} else if (-BOUND_DR_Y_NORM < dirY && dirY < BOUND_DR_Y_NORM) {
		if (diff < 0.33f) {
			character = '"';
		} else if (diff > 0.67f) {
			character = '_';
		} else {
			character = '-';
		}
	} else if (dirX * dirY > 0.0f) {
		if (axis == Axis::Y) {
			if (diff < 0.3) {
				character = '|';
			} else if (diff > 0.7) {
				character = '|';
			} else {
				character = '\\';
			}
		} else {
			if (diff < 0.33f) {
				character = '"';
			} else if (diff > 0.67f) {
				character = '_';
			} else {
				character = '\\';
			}
		}
	} else {
		if (axis == Axis::Y) {
			if (diff < 0.3) {
				character = '|';
			} else if (diff > 0.7) {
				character = '|';
			} else {
				character = '/';
			}
		} else {
			if (diff < 0.33f) {
				character = '"';
			} else if (diff > 0.67f) {
				character = '_';
			} else {
				character = '/';
			}
		}
	}

	return character;
}

bool Map::isOccupied(int y, int x, GameObject* caller, GameObject*& owner) {
	bool collision;

	// Checks if the tile is empty
	if (fieldAt(y, x).character == ' ') {
		collision = false;
	} else {
		// Checks if the tile is occupied by a different game object
		if (fieldAt(y, x).owner != caller) {
			collision = true;
		} else {
			// Checks if the tile is old enough to collide with it
			collision = fieldAt(y, x).age <= gameObjectData[fieldAt(y, x).owner].age - MAX_OCCUPATION_AGE;
		}
	}

	// Returns owner of the tile on collision
	if (collision) {
		owner = fieldAt(y, x).owner;
	}

	return collision;
}

bool Map::isOccupied(int y, int x, GameObject* caller) {
	// Declares dummy variable that gets discarded
	GameObject* owner;
	return isOccupied(y, x, caller, owner);
}

Tile& Map::fieldAt(int y, int x) {
	if (y < 0 || y >= field.size() || x < 0 || x >= field[0].size()) {
		printf("Out of bounds: %d, %d", y, x);
		exit(0);
	}

	return field[y][x];
}

float Map::getInternalHeight() {
	return toInternalCoordsY(field.size());
}

float Map::getInternalWidth() {
	return toInternalCoordsX(field[0].size());
}

bool Map::isRasterizedLinePointOccupied(float pointY, float pointX, float dirY, float dirX, GameObject* caller, GameObject*& owner) {
	// Rasterizes point
	float screenPointY, screenPointX;
	rasterizeLinePoint(pointY, pointX, dirY, dirX, screenPointY, screenPointX);

	// Return true iif corresponding tile is occupied
	return isOccupied((int) screenPointY, (int) screenPointX, caller, owner);
}

bool Map::isRasterizedLinePointOccupied(float pointY, float pointX, float dirY, float dirX, GameObject* caller) {
	// Declares dummy variable that gets discarded
	GameObject* owner;
	return isRasterizedLinePointOccupied(pointY, pointX, dirY, dirX, caller, owner);
}

bool Map::areDistinguishable(float point1Y, float point1X, float dir1Y, float dir1X, float point2Y, float point2X, float dir2Y, float dir2X) {
	// Calculates rasterizated points of the given line points
	float screenPoint1Y, screenPoint1X;
	rasterizeLinePoint(point1Y, point1X, dir1Y, dir1X, screenPoint1Y, screenPoint1X);
	float screenPoint2Y, screenPoint2X;
	rasterizeLinePoint(point2Y, point2X, dir2Y, dir2X, screenPoint2Y, screenPoint2X);

	// Returns true iif the rasterized points are different
	return (int) screenPoint1Y != (int) screenPoint2Y || (int) screenPoint1X != (int) screenPoint2X;
}

void Map::toScreenCoords(float inY, float inX, float& outY, float& outX) {
	outY = toScreenCoordsY(inY);
	outX = toScreenCoordsX(inX);
}

float Map::toScreenCoordsY(float y) {
	return y / TILE_SIZE_Y;
}

float Map::toScreenCoordsX(float x) {
	return x / TILE_SIZE_X;
}

float Map::toScreenCoordsAxis(float value, Axis axis) {
	return value / (axis == Axis::X ? TILE_SIZE_X : TILE_SIZE_Y);
}

void Map::toInternalCoords(float inY, float inX, float& outY, float& outX) {
	outY = toInternalCoordsY(inY);
	outX = toInternalCoordsX(inX);
}

float Map::toInternalCoordsY(float y) {
	return y * TILE_SIZE_Y;
}

float Map::toInternalCoordsX(float x) {
	return x * TILE_SIZE_X;
}

float Map::toInternalCoordsAxis(float value, Axis axis) {
	return value * (axis == Axis::X ? TILE_SIZE_X : TILE_SIZE_Y);
}

float Map::getTileDiagonalSlope() {
	return TILE_SIZE_Y / TILE_SIZE_X;
}

void Map::rasterizeAndDrawLinePoint(float pointY, float pointX, float dirY, float dirX, int color, GameObject* caller, Symbol symbol, bool temp) {
	// Rasterizes point
	float screenPointY, screenPointX;
	Axis axis = rasterizeLinePoint(pointY, pointX, dirY, dirX, screenPointY, screenPointX);

	// Gets correct character for the symbol
	char character;
	switch (symbol) {
		case Symbol::LINE:
			character = getLineChar(dirY, dirX, fmod(axis == Axis::Y ? screenPointX : screenPointY, 1.0f), axis);
			break;
		case Symbol::CRASH:
			character = 'X';
			break;
		case Symbol::TEMP:
			character = '*';
			break;
	}

	// Draws point to the screen
	// XXX Why does this compile? screenPoint is float and draw() takes an int there
	draw(character, screenPointY, screenPointX, color, caller, temp);
}

void Map::drawDot(char character, float pointY, float pointX, int color, GameObject* caller) {
	int screenPointY = toScreenCoordsY(pointY);
	int screenPointX = toScreenCoordsX(pointX);

	draw('_', screenPointY - 1, screenPointX - 1, color, caller);
	draw('_', screenPointY - 1, screenPointX, color, caller);
	draw('_', screenPointY - 1, screenPointX + 1, color, caller);
	draw('(', screenPointY, screenPointX - 1, color, caller);
	draw(character , screenPointY, screenPointX, color, caller);
	draw(')', screenPointY, screenPointX + 1, color, caller);
	draw('"', screenPointY + 1, screenPointX - 1, color, caller);
	draw('"', screenPointY + 1, screenPointX, color, caller);
	draw('"', screenPointY + 1, screenPointX + 1, color, caller);
}

void Map::erase(GameObject* caller) {
	// Iterates over all tiles the caller has drawn to
	for (auto it = draws[caller].begin(); it < draws[caller].end(); ++it) {
		// Checks if the tile is still owned by the caller
		if (it->tile->owner == caller) {
			// Erases the tile
			draw(' ', it->y, it->x, DEFAULT, NO_GAME_OBJECT);
		}
	}

	// Clears the draws vector of the caller
	// TODO
}
