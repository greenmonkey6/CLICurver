edit main.cpp
vsplit main.h
wincmd t
tabnew
edit user.cpp
vsplit user.h
wincmd t
tabnew
edit game.cpp
vsplit game.h
wincmd t
tabnew
edit game_object.cpp
vsplit game_object.h
wincmd t
tabnew
edit player.cpp
vsplit player.h
wincmd t
tabnew
edit map.cpp
vsplit map.h
wincmd t
tabnew
edit item.cpp
vsplit item.h
wincmd t
tabnew
edit keylistener.cpp
vsplit keylistener.h
wincmd t
tabfirst
nnoremap <F5> :!clear; clear; mkdir -p ../build && cd ../build && cmake -DCMAKE_BUILD_TYPE=Release .. && make<CR>
nnoremap <F7> :!clear; clear; mkdir -p ../build && cd ../build && ./CLICurver Alice green a s d Bob red j k l<CR>
" vim: set ft=vim :
