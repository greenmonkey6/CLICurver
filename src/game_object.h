#ifndef _GAME_OBJECT_H
#define _GAME_OBJECT_H

// Forward declaration of map because of circular include
class Map;

class GameObject {
	private:
		/** Map the game object is in */
		Map* map;

	protected:
		/** Position of the game object in the internal coordinate system */
		float posY, posX;

		/**
		 * Returns a pointer to the map the game object is in
		 */
		Map* getMap();

		/**
		 * Returns the position of the game object in the internal coordinate system
		 */
		float getPosY();
		float getPosX();

		/**
		 * Adds to the position of the game object in the internal coordinate system
		 */
		void addPosY(float value);
		void addPosX(float value);

	public:
		explicit GameObject(Map* map);

		/**
		 * Renders the game objects physical state to the screen
		 */
		virtual void render() = 0;

		/**
		 * Calculates and sets new physical state of the game object
		 */
		virtual void update(float delta) = 0;

		virtual ~GameObject() {}
};

#endif
