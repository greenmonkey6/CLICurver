#ifndef _ITEM_H
#define _ITEM_H

#include "map.h"
#include "game_object.h"

// Forward declaration of game because of circular include
class Game;

enum class ItemEffect {
	NO_EFFECT, FAST, SLOW, CLEAN_INSTALL, REVERSE, INVISIBLE, BOLD, THIN
};
// TODO Implement BOLD and THIN (Increase to 7 afterwards)
int const ITEM_EFFECT_NUMBER = 5;

char itemEffectToChar(ItemEffect effect);

class Item : public GameObject {
	private:
		/** Game this item is in. Needed to apply global item effects */
		Game* game;

		/** Effect of this item */
		ItemEffect effect;

		/** Indicates if the item is destroyed. This happens if a player uses it. */
		bool destroyed;

		/** Time in seconds this item shall live before it gets destroyed */
		float const TIME_TO_LIVE;

		/** Age in seconds of the item that is used to determine when the item shall get destroyed */
		float age;

	public:
		explicit Item(Map* map, Game* game, float timeToLive, ItemEffect effect);
		explicit Item(Map* map, Game* game, float timeToLive);

		void render() override;

		void update(float delta) override;

		/**
		 * Return the effect of this item
		 */
		ItemEffect getEffect();

		/**
		 * Applies the effect of the item
		 */
		void applyEffect(GameObject* caller=NO_GAME_OBJECT);

		/**
		 * Destroys the item and removes it from the map
		 */
		void destroy();

		/**
		 * Returns if the item is destroyed.
		 */
		bool isDestroyed();
};

#endif
