#include <cmath>

#include "player.h"
#include "keylistener.h"
#include "main.h"

Player::Player(Map* map, User* user) :
	GameObject(map) {
	this->user = user;

	randomDir(dirY, dirX);

	// Sets speeds
	speed = NORMAL_SPEED;
	rotationalSpeed = NO_ROTATIONAL_SPEED;

	crashed = false;

	invisible = false;
	invisibleCount = 0.0f;
	nextGap = 0.0f;
	nextLine = 0.0f;

	inverted = false;
}

void Player::crash() {
	crashed = true;
	speed = NO_SPEED;
	rotationalSpeed = NO_ROTATIONAL_SPEED;
	invisible = false;
}

void Player::render() {
	Symbol symbol;
	if (!crashed) {
		if (invisible) {
			symbol = Symbol::TEMP;
		} else {
			symbol = Symbol::LINE;
		}
	} else {
		symbol = Symbol::CRASH;
	}

	getMap()->rasterizeAndDrawLinePoint(getPosY(), getPosX(), dirY, dirX, user->getColor(), this, symbol, invisible);

	// TODO Add rasterization for movement faster than the framerate
}

inline void Player::resetInvisibleCount(float nextGapMin, float nextGapMax, float nextLineMin, float nextLineMax) {
	invisibleCount = 0.0f;
	nextGap = randomFloat(nextGapMin, nextGapMax);
	nextLine = randomFloat(nextLineMin, nextLineMax);
}

void Player::update(float delta) {
	if (!crashed) {
		// XXX Only controls get inverted but not the current rotation direction
		// Inverts the controls of the player
		int leftKey, straightKey, rightKey;
		if (inverted) {
			leftKey = user->getRightKey();
			straightKey = user->getStraightKey();
			rightKey = user->getLeftKey();
		} else {
			leftKey = user->getLeftKey();
			straightKey = user->getStraightKey();
			rightKey = user->getRightKey();
		}

		// Checks if the user moves the player
		if (isKeyPressed(leftKey)) {
			rotationalSpeed = -NORMAL_ROTATIONAL_SPEED;
		} else if (isKeyPressed(straightKey)) {
			rotationalSpeed = NO_ROTATIONAL_SPEED;
		} else if (isKeyPressed(rightKey)) {
			rotationalSpeed = NORMAL_ROTATIONAL_SPEED;
		}

		// Calculates player's new direction
		float angle = delta * rotationalSpeed;
		dirY = std::sin(angle) * dirX + cos(angle) * dirY;
		dirX = std::cos(angle) * dirX - sin(angle) * dirY;

		// XXX It should be possible to do this cleaner
		// Renormalizes
		float dirLength = sqrt(dirY * dirY + dirX * dirX);
		dirY /= dirLength;
		dirX /= dirLength;

		// Calculates player's new position
		addPosY(delta * dirY * speed);
		addPosX(delta * dirX * speed);

		// Detects a collision of the player with a game object
		GameObject* owner;
		bool collision = getMap()->isRasterizedLinePointOccupied(getPosY(), getPosX(), dirY, dirX, this, owner);
		if (collision) {
			if (Player* player = dynamic_cast<Player*>(owner)) {
				if (!invisible) {
					crash();
				}
			} else if (Item* item = dynamic_cast<Item*>(owner)) {
				item->applyEffect(this);
				item->destroy();
			} else {
				crash();
			}
		}

		// Makes the player invisible
		invisibleCount += delta;

		if (invisibleCount > nextLine) {
			invisible = true;
			if (invisibleCount > nextLine + nextGap) {
				invisible = false;
				resetInvisibleCount(0.1f, 1.0f, 1.0f, 5.0f);
			}
		}

		// Alters effect timers
		// TODO Visually show when a effect is running out
		for (auto it = itemEffectCounts.begin(); it != itemEffectCounts.end(); ++it) {
			float old = it->second;

			it->second = std::max(it->second - delta, 0.0f);

			if (old != it->second && it->second == 0.0f) {
				removeItemEffect(it->first);
			}
		}
	}
}

bool Player::isCrashed() {
	return crashed;
}

User* Player::getUser() {
	return user;
}

void Player::applyItemEffect(ItemEffect itemEffect) {
	switch (itemEffect) {
		case ItemEffect::NO_EFFECT:
			// Does nothing
		case ItemEffect::FAST:
			if (itemEffectCounts[ItemEffect::SLOW] > 0) {
				removeItemEffect(ItemEffect::SLOW);
			} else {
				speed *= FAST_SPEED_MULT;
				itemEffectCounts[itemEffect] = 5.0f;
			}
			break;
		case ItemEffect::SLOW:
			if (itemEffectCounts[ItemEffect::FAST] > 0) {
				removeItemEffect(ItemEffect::FAST);
			} else {
				speed *= SLOW_SPEED_MULT;
				itemEffectCounts[itemEffect] = 5.0f;
			}
			break;
		case ItemEffect::CLEAN_INSTALL:
			getMap()->erase(this);
			break;
		case ItemEffect::BOLD:
			// TODO
			break;
		case ItemEffect::THIN:
			// TODO
			break;
		case ItemEffect::REVERSE:
			inverted = true;
			itemEffectCounts[itemEffect] = 5.0f;
			break;
		case ItemEffect::INVISIBLE:
			// Makes the player invisible
			// (This effect basically just makes a big hole in the line)
			resetInvisibleCount(5.0f, 5.0f, 0.0f, 0.0f);
			// Sets item effect count only for graphical purposes
			itemEffectCounts[itemEffect] = 5.0f;
			break;
	}
}

void Player::removeItemEffect(ItemEffect itemEffect) {
	itemEffectCounts[itemEffect] = 0.0f;
	switch (itemEffect) {
		case ItemEffect::NO_EFFECT:
			// Does nothing
		case ItemEffect::FAST:
			speed = NORMAL_SPEED;
			break;
		case ItemEffect::SLOW:
			speed = NORMAL_SPEED;
			break;
		case ItemEffect::CLEAN_INSTALL:
			// Does nothing
			break;
		case ItemEffect::BOLD:
			// TODO
			break;
		case ItemEffect::THIN:
			// TODO
			break;
		case ItemEffect::REVERSE:
			inverted = false;
			break;
		case ItemEffect::INVISIBLE:
			// Does nothing, since the invisible count already resets itself in update()
			break;
	}
}
