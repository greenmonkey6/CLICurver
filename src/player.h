#ifndef _PLAYER_H
#define _PLAYER_H

#include "map.h"
#include "user.h"
#include "item.h"
#include "game_object.h"

float const NO_SPEED = 0.0f;
float const NORMAL_SPEED = 10.0f;
float const SLOW_SPEED_MULT = 0.5f;
float const FAST_SPEED_MULT = 2.0f;

float const NO_ROTATIONAL_SPEED = 0.0f;
float const NORMAL_ROTATIONAL_SPEED = 2.0f;

class Player : public GameObject {
	private:
		/** User the player is controlled by */
		User* user;

		/** Moving direction of the player. The vector (dirX, dirY)^T has to be normalized. */
		float dirY, dirX;

		/** Speed of the player in fields per second */
		float speed;
		/** Rotational speed of the player in radians per second. This is used to turn the player when the left or right key is pressed. */
		float rotationalSpeed;

		/** Indicates if the player is crashed into a player or the wall */
		bool crashed;

		/** Indicates if the player is currently invisible */
		bool invisible;
		float invisibleCount;
		float nextGap;
		float nextLine;

		/** Indicates if the player's controls are currently inverted */
		bool inverted;

		// XXX Assumes that the float gets initialized with 0.0f
		/** Stores the counts for each effect */
		std::map<ItemEffect, float> itemEffectCounts;

		/**
		 * Sets the player into 'crashed' mode and stops it
		 */
		void crash();

		/**
		 * Small helper function to reset the invisible count
		 */
		inline void resetInvisibleCount(float nextGapMin, float nextGapMax, float nextLineMin, float nextLineMax);

	public:
		Player(Map* map, User* user);

		void render() override;

		void update(float delta) override;

		/**
		 * Returns if the player crashed into a player or wall
		 */
		bool isCrashed();

		/**
		 * Returns the user that controlls the player
		 */
		User* getUser();

		/**
		 * Applies the given item effect to this player
		 */
		void applyItemEffect(ItemEffect itemEffect);

		/**
		 * Removes the given item effect to this player
		 */
		void removeItemEffect(ItemEffect itemEffect);
};

#endif
