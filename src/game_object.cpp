#include "game_object.h"
#include "main.h"
#include "map.h"

Map* GameObject::getMap() {
	return map;
}

float GameObject::getPosY() {
	return posY;
}

float GameObject::getPosX() {
	return posX;
}

void GameObject::addPosY(float value) {
	posY += value;
}

void GameObject::addPosX(float value) {
	posX += value;
}

GameObject::GameObject(Map* map) {
	this->map = map;

	// Generates random position
	posY = randomFloat(0.1f * getMap()->getInternalHeight(), 0.9f * getMap()->getInternalHeight());
	posX = randomFloat(0.1f * getMap()->getInternalWidth(), 0.9f * getMap()->getInternalWidth());
}
