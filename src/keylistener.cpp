#include <ncurses.h>
#include <vector>

#include "keylistener.h"

bool keys[512] = {};

bool running = true;

void startKeyListener() {
	// Starts listening for key presses
	while (running) {
		keys[(int) getch()] = true;
	}
}

void stopKeyListener() {
	running = false;
}

bool isKeyPressed(char key) {
	bool temp = keys[(int) key];
	keys[(int) key] = false;
	return temp;
}

void resetKeyListener() {
	for (size_t i = 0; i < 512; i++) {
		isKeyPressed((char) i);
	}
}
