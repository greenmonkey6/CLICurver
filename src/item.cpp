#include <random>
#include <cmath>

#include "item.h"
#include "main.h"
#include "player.h"
#include "game.h"

char itemEffectToChar(ItemEffect effect) {
	switch(effect) {
		case ItemEffect::NO_EFFECT:
			return 'N';
		case ItemEffect::FAST:
			return 'F';
		case ItemEffect::SLOW:
			return 'S';
		case ItemEffect::CLEAN_INSTALL:
			return 'C';
		case ItemEffect::BOLD:
			return 'B';
		case ItemEffect::THIN:
			return 'T';
		case ItemEffect::REVERSE:
			return 'R';
		case ItemEffect::INVISIBLE:
			return 'I';
	}
}

Item::Item(Map* map, Game* game, float timeToLive, ItemEffect effect) :
	GameObject(map),
	TIME_TO_LIVE(timeToLive) {
	this->game = game;
	age = 0.0f;
	this->effect = effect;
	destroyed = false;
}

ItemEffect randomItemEffect() {
	return static_cast<ItemEffect>(randomInt(1, ITEM_EFFECT_NUMBER));
}

Item::Item(Map* map, Game* game, float timeToLive) :
	Item(map, game, timeToLive, randomItemEffect()) {
}

void Item::render() {
	if (!destroyed) {
		getMap()->drawDot(itemEffectToChar(effect), getPosY(), getPosX(), BLUE, this);
	} else {
		getMap()->erase(this);
	}
}

void Item::update(float delta) {
	age += delta;

	if (age >= TIME_TO_LIVE) {
		destroy();
	}
}

ItemEffect Item::getEffect() {
	if (!destroyed) {
		return effect;
	} else {
		return ItemEffect::NO_EFFECT;
	}
}

void Item::applyEffect(GameObject* caller) {
	Player* player = dynamic_cast<Player*>(caller);
	switch(effect) {
		case ItemEffect::NO_EFFECT:
			// Does nothing
			break;
		case ItemEffect::FAST:
			if (player) player->applyItemEffect(effect);
			break;
		case ItemEffect::SLOW:
			if (player) player->applyItemEffect(effect);
			break;
		case ItemEffect::CLEAN_INSTALL:
			{
				// Applies clean install to all players
				// TODO Maybe directly call map's erase() method from here
				std::vector<Player*> players = game->getPlayers();
				for (Player* p : players) {
					p->applyItemEffect(effect);
				}
			}
			break;
		case ItemEffect::BOLD:
			if (player) player->applyItemEffect(effect);
			break;
		case ItemEffect::THIN:
			if (player) player->applyItemEffect(effect);
			break;
		case ItemEffect::REVERSE:
			if (player) player->applyItemEffect(effect);
			break;
		case ItemEffect::INVISIBLE:
			if (player) player->applyItemEffect(effect);
			break;
	}
}

void Item::destroy() {
	destroyed = true;
}

bool Item::isDestroyed() {
	return destroyed;
}
