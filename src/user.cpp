#include "user.h"

User::User(std::string name, int color, char left, char straight, char right) {
	this->name = name;

	this->color = color;

	this->left = left;
	this->straight = straight;
	this->right = right;

	score = 0;
}

std::string User::getName() {
	return name;
}

int User::getColor() {
	return color;
}

char User::getLeftKey() {
	return left;
}

char User::getStraightKey() {
	return straight;
}

char User::getRightKey() {
	return right;
}

int User::getScore() {
	return score;
}

void User::addScore(int score) {
	this->score += score;
}
