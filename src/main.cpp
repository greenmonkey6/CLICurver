#include <ncurses.h>
#include <vector>
#include <thread>
#include <iostream>
#include <random>
#include <cstring>

#include "main.h"
#include "game.h"
#include "keylistener.h"

std::random_device rd;
std::mt19937 eng(rd());

float randomFloat(float min, float max) {
	std::uniform_real_distribution<> dist(min, max);
	return dist(eng);
}
int randomInt(int min, int max) {
	std::uniform_int_distribution<> dist(min, max);
	return dist(eng);
}

void randomDir(float& dirY, float& dirX) {
	std::uniform_real_distribution<> distDir(-1, 1);
	std::uniform_int_distribution<> distBool(0, 1);
	dirY = distDir(eng);
	dirX = (distBool(eng) * 2 - 1) * std::sqrt(1 - dirY * dirY);
}

void printStats(WINDOW* statsWindow, std::vector<User>* users) {
	wattron(statsWindow, COLOR_PAIR(DEFAULT));
	mvwprintw(statsWindow, 1, 0, "Players");
	wattroff(statsWindow, COLOR_PAIR(DEFAULT));

	int i = 3;
	for (auto it = users->begin(); it < users->end(); ++it) {
		wattron(statsWindow, COLOR_PAIR(it->getColor()));
		mvwprintw(statsWindow, i, 0, "%s", it->getName().c_str());
		wattroff(statsWindow, COLOR_PAIR(it->getColor()));

		wattron(statsWindow, COLOR_PAIR(DEFAULT));
		mvwprintw(statsWindow, i, 10, "%d", it->getScore());
		wattroff(statsWindow, COLOR_PAIR(DEFAULT));
		i++;
	}
	wrefresh(statsWindow);
}

std::vector<std::string> colors{
	"",
	"default",
	"black",
	"red",
	"green",
	"yellow",
	"blue",
	"magenta",
	"cyan",
	"white"
};

int getColor(std::string colorName) {
	for (auto it = colors.begin(); it < colors.end(); ++it) {
		if (*it == colorName) {
			return it - colors.begin();
		}
	}

	return NO_COLOR;
}

void printUsage() {
	std::cout << "Usage: [-h|player-name color left-key straight-key right-key ...]" << std::endl;
	std::cout << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "\t-h, --help" << std::endl;
	std::cout << "\t\tPrints this help text." << std::endl;
	std::cout << std::endl;
	std::cout << "Colors:" << std::endl;
	std::cout << "\tdefault" << std::endl;
	std::cout << "\tblack" << std::endl;
	std::cout << "\tred" << std::endl;
	std::cout << "\tgreen" << std::endl;
	std::cout << "\tyellow" << std::endl;
	std::cout << "\tblue" << std::endl;
	std::cout << "\tmagenta" << std::endl;
	std::cout << "\tcyan" << std::endl;
	std::cout << "\twhite" << std::endl;
}

void exitNCurses() {
	// Makes cursor visible again
	curs_set(1);

	// Deallocates memory and ends
	endwin();
}

int main(int argc, char** argv) {
	// Initializes screen, sets up memory and clears the screen
	initscr();
	
	// Sets up ncurses
	cbreak();
	noecho();
	refresh();

	// Checks if the terminal supports color
	if (!has_colors()) {
		exitNCurses();
		std::cout << "The terminal does not support color" << std::endl;
		return 1;
	}
	start_color();
	use_default_colors();

	// Defines colors
	init_pair(1, -1, -1);
	init_pair(2, COLOR_BLACK, -1);
	init_pair(3, COLOR_RED, -1);
	init_pair(4, COLOR_GREEN, -1);
	init_pair(5, COLOR_YELLOW, -1);
	init_pair(6, COLOR_BLUE, -1);
	init_pair(7, COLOR_MAGENTA, -1);
	init_pair(8, COLOR_CYAN, -1);
	init_pair(9, COLOR_WHITE, -1);
	init_pair(10, -1, COLOR_BLACK);
	init_pair(11, -1, COLOR_RED);
	init_pair(12, -1, COLOR_GREEN);
	init_pair(13, -1, COLOR_YELLOW);
	init_pair(14, -1, COLOR_BLUE);
	init_pair(15, -1, COLOR_MAGENTA);
	init_pair(16, -1, COLOR_CYAN);
	init_pair(17, -1, COLOR_WHITE);

	// Prints help text
	if (argc >= 2 && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)) {
		exitNCurses();
		printUsage();
		return 0;
	}

	// Checks if the correct number arguments is given
	if (argc <= 1 || argc % 5 != 1) {
		exitNCurses();
		std::cout << "Wrong number of arguments!" << std::endl;
		std::cout << std::endl;
		printUsage();
		return 2;
	}

	// Creates users from command line arguments
	// TODO Add GUI to create users
	std::vector<User> users;
	for (int i = 1; i < argc; i += 5) {
		std::string name = argv[i];

		int color = getColor(argv[i + 1]);
		if (color == NO_COLOR) {
			exitNCurses();
			std::cout << "Unknown color: " << argv[i + 1] << std::endl;
			return 3;
		}

		char leftKey = argv[i + 2][0];
		if (argv[i + 2][1] != '\0') {
			exitNCurses();
			std::cout << "Not a character: " << argv[i + 2] << std::endl;
			return 4;
		}

		char straightKey = argv[i + 3][0];
		if (argv[i + 3][1] != '\0') {
			exitNCurses();
			std::cout << "Not a character: " << argv[i + 3] << std::endl;
			return 4;
		}

		char rightKey = argv[i + 4][0];
		if (argv[i + 4][1] != '\0') {
			exitNCurses();
			std::cout << "Not a character: " << argv[i + 4] << std::endl;
			return 4;
		}

		// Adds user
		users.push_back(User(name, color, leftKey, straightKey, rightKey));
	}

	// Gets screen size
	int height, width;
	getmaxyx(stdscr, height, width);

	// Sets up windows
	int statsWidth = 15;
	WINDOW* statsWindow = newwin(height, statsWidth, 0, 0);
	WINDOW* gameWindow = newwin(height, width - statsWidth, 0, statsWidth);
	refresh();

	// Starts key listener in new thread
	std::thread t(startKeyListener);

	printStats(statsWindow, &users);
	while (true) {
		// Initializes game
		Game game(gameWindow, &users);
	
		// Starts and runs main loop
		game.run();

		printStats(statsWindow, &users);
	}

	// Stops and waits for key listener
	stopKeyListener();
	t.join();

	exitNCurses();
	return 0;
}
