#ifndef _GAME_H
#define _GAME_H

#include <vector>
#include <ncurses.h>

#include "player.h"
#include "item.h"
#include "map.h"
#include "user.h"
//#include "game_object.h"

int const NO_RANK = -1;

class Game {
	private:
		/** Indicates if the game is currently running */
		bool running;

		/** NCurses window to render the game on */
		WINDOW* window;

		/** Map the players move in */
		Map map;

		/** Multiplier for game time relative to Wallclock time */
		float const GAME_SPEED;

		/** List of all players in the game */
		std::vector<Player> players;

		/** List of all items currently in the game */
		std::vector<Item> items;

	public:
		explicit Game(WINDOW* window, std::vector<User>* users);

		/**
		 * Starts and runs the game
		 */
		void run();

		/**
		 * Returns a list of pointers to all players in the game
		 */
		std::vector<Player*> getPlayers();
};

#endif
