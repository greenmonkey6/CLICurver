#ifndef _KEYLISTENER_H
#define _KEYLISTENER_H

/**
 * Starts a loop that catches a key presses and buffers them in a non-blocking manner
 */
void startKeyListener();

/**
 * Stops key listening loop
 */
void stopKeyListener();

/**
 * Returns if the given key was lately pressed and clears this buffer
 */
bool isKeyPressed(char key);

/**
 * Resets the buffer of the key listener
 */
void resetKeyListener();

#endif
