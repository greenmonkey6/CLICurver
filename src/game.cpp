#include <chrono>
#include <thread>
#include <map>

#include "game.h"
#include "keylistener.h"

Game::Game(WINDOW* window, std::vector<User>* users) :
	map(window, users->size()),
	GAME_SPEED(1.0f) {
	this->window = window;

	running = true;

	// Instantiates players
	for (auto it = users->begin(); it < users->end(); ++it) {
		players.push_back(Player(&map, &*it));
	}
}

inline int getTime() {
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void Game::run() {
	// Makes cursor invisible
	curs_set(0);

	// Clears buffer of key listener
	resetKeyListener();

	// Map that saves for every player which rank it scored
	std::map<Player*, int> playerRanks;
	for (auto it = players.begin(); it < players.end(); ++it) {
		playerRanks[&*it] = NO_RANK;
	}
	int rankCount = 0;

	float itemCount = 0.0f;

	// Initializes variables for time calculation inside of the loop
	int thisFrame = 0;
	int lastFrame = getTime();
	float delta = 0.0f;
	while (running) {
		// Calculates time that passed since last iteration
		thisFrame = getTime();
		delta = (thisFrame - lastFrame) * 0.001f * GAME_SPEED;
		lastFrame = thisFrame;

		// Spawns items
		// TODO Better spawning algorithm
		itemCount += delta;
		if (itemCount >= 15.0f) {
			itemCount = 0.0f;
			items.push_back(Item(&map, this, 30.0f));
		}

		// Updates and renders all items
		for (auto it = items.begin(); it < items.end(); ++it) {
			it->update(delta);
			it->render();
		}

		// Updates and renders all players
		int notCrashed = 0;
		for (auto it = players.begin(); it < players.end(); ++it) {
			// Updates and renders player
			it->update(delta);
			it->render();

			// TODO Maybe catch case where two players collide in the same iteration
			// Gives crashed player a rank
			if (it->isCrashed() && playerRanks[&*it] == NO_RANK) {
				playerRanks[&*it] = rankCount;
				rankCount++;
			}

			notCrashed += !it->isCrashed();
		}

		// Stops game if all but one player crashed
		if (notCrashed <= 1) {
			running = false;
			for (auto it = players.begin(); it < players.end(); ++it) {
				if (playerRanks[&*it] == NO_RANK) {
					playerRanks[&*it] = rankCount;
				}
			}
		}

		// Refreshes screen
		wrefresh(window);

		// Syncs the game's refresh rate to roughly 60 Hz (indepentend of game speed)
		std::this_thread::sleep_for(std::chrono::milliseconds(16));
	}

	// Adds score for all player's users
	for (auto it = players.begin(); it < players.end(); ++it) {
		// TODO Add better function for score calculation
		int score = playerRanks[&*it];
		it->getUser()->addScore(score);
	}

	// Cleans game screen
	werase(window);

	// Makes cursor visible again
	curs_set(1);
}


std::vector<Player*> Game::getPlayers() {
	std::vector<Player*> playerPointers;

	for (auto it = players.begin(); it < players.end(); ++it) {
		playerPointers.push_back(&*it);
	}

	return playerPointers;
}
