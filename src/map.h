#ifndef _MAP_H
#define _MAP_H

#include <ncurses.h>
#include <vector>
#include <map>

#include "game_object.h"

enum class Axis {
	X, Y
};

enum class Symbol {
	LINE, CRASH, TEMP
};

struct Tile {
	/** Character on the screen */
	char character;
	/** Pointer to the game object that occupies the tile */
	GameObject* owner;
	/** Index that indicates how many new tiles were occupied by the game object before. */
	int age;
	/** Color of the character */
	int color;
};

// TODO Maybe merge Position and TileRef
struct Position {
	/** Pointer to the game object that occupies the tile */
	GameObject* owner;
	/** Coordinates of this position */
	int y, x;
};

struct TileRef {
	/** Pointer to the tile */
	Tile* tile;
	/** Coordinates of the tile */
	int y, x;
};

struct GameObjectData {
	/** Counter for draws a player made on new tiles. A new tile is a tile the player never occupied before. */
	int age = 0;
};

// XXX Maybe make a dummy instance of existing class or create new class for this
GameObject* const NO_GAME_OBJECT = NULL;

// TODO Maybe even introduce an interface for game objects to store owners, because the map does not need to see anything of the functionality of a game object. It could also hold the game objects color. NO_GAME_OBJECT could then be an instance of a Wall class.
// TODO Use data types to distinguish between internal and screen coordinates
// TODO Create data types for points and lines
class Map {
	private:
		/** Size of a tile on the map measured in the interal coordinate system */
		float const TILE_SIZE_Y;
		float const TILE_SIZE_X;

		int const MAX_OCCUPATION_AGE;
		
		/** NCurses window the map is rendered on */
		WINDOW* window;

		/** Two dimensional array of tiles that represent the whole map */
		std::vector< std::vector<Tile> > field;

		/** List that stores data for every game object */
		std::map<GameObject*, GameObjectData> gameObjectData;

		/**
		 * List of tiles that should get cleared again if that game object draws again
		 */
		std::vector<Position> temps;

		/**
		 * Stores for every game objects a list of all tiles it draw to in chronological order
		 */
		std::map<GameObject*, std::vector<TileRef>> draws;

		/** Small helper function that draw the given character to the given position in the given color */
		inline void drawChar(char character, int y, int x, int color);

		/** Draws the given character form a game object at the given point in screen coordinates and ages the tile depending on game object data */
		void draw(char character, int y, int x, int color, GameObject* caller=NO_GAME_OBJECT, bool temp=false);

		/**
		 * Calculates the corresponding point in internal coordinates in a rastarization of the whole line. Returns the axis at which the rasterization was done along.
		 */
		Axis rasterizeLinePoint(float inY, float inX, float dirY, float dirX, float& outY, float& outX);

		/**
		 * Returns a character that matches a line in the given direction and the given distance in the given axis from the middle of a tile.
		 */
		char getLineChar(float dirY, float dirX, float diff, Axis axis);

		/**
		 * Decides if the given point in screen coordinates on the map is occupied by a game object. The decision is also based on the age of the tile. So a game object cannot collide with a tile that was occupied in the near past. The owner of the tile gets also returned on a collision (otherwise not).
		 */
		bool isOccupied(int y, int x, GameObject* caller, GameObject*& owner);
		bool isOccupied(int y, int x, GameObject* caller);

		/**
		 * Small helper function to savely access the field
		 */
		Tile& fieldAt(int y, int x);

	public:
		explicit Map(WINDOW* window, int playerCount);

		/**
		 * Return height of the map in internal coordinates
		 */
		float getInternalHeight();
		/**
		 * Returns width of the map in internal coordinates
		 */
		float getInternalWidth();

		/**
		 * Decides if the given line point in internal coordinates maps to a occupied tile in screen coordinates. The owner of the tile gets also returned on a collision (otherwise not).
		 */
		bool isRasterizedLinePointOccupied(float inY, float inX, float dirY, float dirX, GameObject* caller);
		bool isRasterizedLinePointOccupied(float inY, float inX, float dirY, float dirX, GameObject* caller, GameObject*& owner);

		/**
		 * Decides if the two given points in internal coordinates of two lines are in two different tiles on the screen
		 */
		bool areDistinguishable(float point1Y, float point1X, float dir1Y, float dir1X, float point2Y, float point2X, float dir2Y, float dir2X);

		/**
		 * Converts the given internal coordinates to the corresponding coordinates on the screen
		 */
		void toScreenCoords(float inY, float inX, float& outY, float& outX);
		float toScreenCoordsY(float y);
		float toScreenCoordsX(float x);
		float toScreenCoordsAxis(float value, Axis axis);

		/**
		 * Converts the given screen coordinates to the corresponding coordinates in the internal coordinate system
		 */
		void toInternalCoords(float inY, float inX, float& outY, float& outX);
		float toInternalCoordsY(float y);
		float toInternalCoordsX(float x);
		float toInternalCoordsAxis(float value, Axis axis);

		/** Returns the slope of a diagonal in a tile measured in the internal coordinate system */
		float getTileDiagonalSlope();

		/**
		 * Calculates and draws the corresponding point in internal coordinates in a rastarization of the whole line
		 */
		void rasterizeAndDrawLinePoint(float pointY, float pointX, float dirY, float dir, int color, GameObject* caller=NO_GAME_OBJECT, Symbol symbol=Symbol::LINE, bool temp=false);

		/**
		 * Draws a character in a dot at the point in internal coordinates
		 */
		void drawDot(char character, float pointY, float pointX, int color, GameObject* caller=NO_GAME_OBJECT);

		/**
		 * Erases all tiles from this game object
		 */
		void erase(GameObject* caller);
};

#endif
