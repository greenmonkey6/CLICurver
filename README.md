# CLICurver

A NCurses implementation of the game *Curve Fever* or *Achtung die Kurve!*

![Imgur](https://i.imgur.com/PNRsArY.png)

## Dependencies

 * `NCurses`

## Installation

```sh
$ take build
$ cmake ..
$ make -j$(nproc)
```
